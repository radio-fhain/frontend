const token = import.meta.env.VITE_STRAPI_TOKEN

export async function get () {
  const res = await fetch(`${import.meta.env.VITE_API_URL}/api/pages/home`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const data = await res.json()

  if (data) {
    return {
      body: {
        pages: data
      }
    }
  } else {
    console.log('res')
  }
}
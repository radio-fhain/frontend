const token = import.meta.env.VITE_STRAPI_TOKEN

export async function get ({ params }) {
  const { page } = params

  console.log(page, params)

  const res = await fetch(`${import.meta.env.VITE_API_URL}/api/pages?filters[UID][$eq]=${page}`, {
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
  const data = await res.json()

  if (data) {
    return {
      body: {
        page: data
      }
    }
  } else {
    console.log(res)
  }
}
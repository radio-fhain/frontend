import adapter from '@sveltejs/adapter-netlify';
import path from 'path'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter()
	},
  vite: {
    resolve: {
      alias: {
        $load: path.resolve("./src/load"),
        $components: path.resolve("./src/components"),
        $stores: path.resolve("./src/stores"),
        $transitions: path.resolve("./src/transitions"),
        $functions: path.resolve("./src/functions"),
        $utilities: path.resolve("./utilities")
      }
    }
  }
};

export default config;

module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: { extend: {
    fontFamily: {
      display: ['PicNic'],
      sans: ['Inter']
    }
  } },
	plugins: []
};
